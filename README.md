# App diseños de Flutter

## Introduction

> Aqui estan los siguientes diseños:
 - Cuadrado animado
 ![cuadro_animado](gifs/cuadro_animado.gif)
 - Pagina de emergencias
 ![emergencias](gifs/emergency.gif)
 - Graficas circulares
 ![circular_progress](gifs/circular_progress.gif)
 - Headers (Custom Painter)
 ![headers](gifs/headers.jpg)
 - Pinterest
 ![pinterest](gifs/pinterest.gif)
 - Sildeshow
 ![slideshow](gifs/slideshow.gif)
 - Sliver List Personalizado
 ![slideshow](gifs/sliver_list_page.gif)

## Code Samples

> Esta app usa Flutter 1.17.5