// import 'package:diseno_app/src/labs/slideshow_page.dart';
// import 'package:diseno_app/src/pages/animaciones_page.dart';
// import 'package:diseno_app/src/pages/emergency_page.dart';
// import 'package:diseno_app/src/pages/graficas_circulares_page.dart';
// import 'package:diseno_app/src/pages/headers_page.dart';
// import 'package:diseno_app/src/pages/pinterest_page.dart';
// import 'package:diseno_app/src/widgets/slideshow.dart';
import 'package:diseno_app/src/models/layout_model.dart';
import 'package:diseno_app/src/pages/launcher_page.dart';
import 'package:diseno_app/src/pages/launcher_tablet_page.dart';
import 'package:diseno_app/src/pages/sliver_list_page.dart';
import 'package:diseno_app/src/pages/theme/theme_changer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// void main() => runApp(
//   MultiProvider(
//     providers: [
//       Provider(create: (_) => new ThemeChanger(1)),
//       Provider(create: (_) => new LayoutModel()),
//     ],
//     child: MyApp()));
void main() => runApp(
  ChangeNotifierProvider(
    create: (_) => new LayoutModel(),
      child: ChangeNotifierProvider(
      create: (_) => new ThemeChanger(1),
      child: MyApp()),
  ));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final currentTheme = Provider.of<ThemeChanger>(context).currentTheme;
    return MaterialApp(
      theme: currentTheme,
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      home: OrientationBuilder(
        builder: (BuildContext context, Orientation orientation) {
          final screenSize = MediaQuery.of(context).size;
          // Si es mayor a 500 pixeles es porque es bastante ancha
          return (screenSize.width > 500) ? LauncherTabletPage() : LauncherPage();
          // return Container(
          // child: LauncherPage(),
          // );
        },
      ),
    );
  }
}