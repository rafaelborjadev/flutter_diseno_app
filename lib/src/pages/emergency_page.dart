import 'package:animate_do/animate_do.dart';
import 'package:diseno_app/src/widgets/boton_gordo.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../widgets/headers.dart';

class ItemBoton {

  final IconData icon;
  final String texto;
  final Color color1;
  final Color color2;

  ItemBoton( this.icon, this.texto, this.color1, this.color2 );
}

class EmergencyPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final items = <ItemBoton>[
      new ItemBoton( FontAwesomeIcons.carCrash, 'Motor Accident', Color(0xff6989F5), Color(0xff906EF5) ),
      new ItemBoton( FontAwesomeIcons.plus, 'Medical Emergency', Color(0xff66A9F2), Color(0xff536CF6) ),
      new ItemBoton( FontAwesomeIcons.theaterMasks, 'Theft / Harrasement', Color(0xffF2D572), Color(0xffE06AA3) ),
      new ItemBoton( FontAwesomeIcons.biking, 'Awards', Color(0xff317183), Color(0xff46997D) ),
      new ItemBoton( FontAwesomeIcons.carCrash, 'Motor Accident', Color(0xff6989F5), Color(0xff906EF5) ),
      new ItemBoton( FontAwesomeIcons.plus, 'Medical Emergency', Color(0xff66A9F2), Color(0xff536CF6) ),
      new ItemBoton( FontAwesomeIcons.theaterMasks, 'Theft / Harrasement', Color(0xffF2D572), Color(0xffE06AA3) ),
      new ItemBoton( FontAwesomeIcons.biking, 'Awards', Color(0xff317183), Color(0xff46997D) ),
      new ItemBoton( FontAwesomeIcons.carCrash, 'Motor Accident', Color(0xff6989F5), Color(0xff906EF5) ),
      new ItemBoton( FontAwesomeIcons.plus, 'Medical Emergency', Color(0xff66A9F2), Color(0xff536CF6) ),
      new ItemBoton( FontAwesomeIcons.theaterMasks, 'Theft / Harrasement', Color(0xffF2D572), Color(0xffE06AA3) ),
      new ItemBoton( FontAwesomeIcons.biking, 'Awards', Color(0xff317183), Color(0xff46997D) ),
    ];
    List<Widget> itemMap = 
    items.map((item) => FadeInLeft(

          child: BotonGordo(
        color1: item.color1,
        color2: item.color2,
        icon: item.icon,
        texto: item.texto,
        onPress: (){print('Hola');},
      ),
    )).toList();

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 300),
            child: ListView(
              // Para el efecto de rebote en listas
              physics: BouncingScrollPhysics(),
              children: <Widget>[
              ...itemMap
              ],
            ),
          ),
          _Encabezado(),
        ],
      ),
   );
  }
}

class _Encabezado extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        IconHeader(
          icon: FontAwesomeIcons.plus, 
          titulo: 'Asistencia Medica', 
          subtitulo: 'Haz solicitado', 
          color1: Color(0xff536CF6), 
          color2: Color(0xff66A9F2)
          ),
          Positioned(
            right: 10,
            top: 40,
            child: RawMaterialButton(
              child: FaIcon(FontAwesomeIcons.ellipsisV,color: Colors.white),
              onPressed: (){},
              shape: CircleBorder(),
              )
          )
      ],
    );
  }
}

class _BotonGordoTemp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BotonGordo(
        color1: Color(0xff6989f5),
        color2: Color(0xff906EF5),
        texto: 'Motor Accident',
        icon: FontAwesomeIcons.carCrash,
        onPress: () => {print('Hola mundo')},
      )
    );
  }
}

class PageHeader extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return IconHeader(
      color1: Color(0xff526BF6),
      color2: Color(0xff67ACF2),
      icon: FontAwesomeIcons.plus,
      titulo: 'Asistencia Medica',
      subtitulo: 'Haz Solicitado',
    );
  }
}