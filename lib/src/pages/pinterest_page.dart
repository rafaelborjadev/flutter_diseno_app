import 'package:diseno_app/src/pages/theme/theme_changer.dart';
import 'package:diseno_app/src/widgets/pinterest_menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';


class PinterestPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
  final width = MediaQuery.of(context).size.width;
    return ChangeNotifierProvider(
      create: (_) => new _MenuModel(),
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            PinterestGrid(),
            _PinterestMenuLocation(width: width)
          ],
        ),
   ),
    );
  }
}

class _PinterestMenuLocation extends StatelessWidget {
  const _PinterestMenuLocation({
    Key key,
    @required this.width,
  }) : super(key: key);

  final double width;

  @override
  Widget build(BuildContext context) {
    final mostrar = Provider.of<_MenuModel>(context).mostrar;
    final appTheme = Provider.of<ThemeChanger>(context).currentTheme;
    return Positioned(
      child: Container(
        height: 100,
        width: width,
        child: Align(
          child: PinterestMenu(
            items: [
              PinterestButton(icon: Icons.pie_chart, onPressed: (){print('Icon pie chart');}),
              PinterestButton(icon: Icons.search, onPressed: (){print('Icon search');}),
              PinterestButton(icon: Icons.notifications, onPressed: (){print('Icon notification');}),
              PinterestButton(icon: Icons.supervised_user_circle, onPressed: (){print('Icon Supervised');})
            ],
            mostrar: mostrar, 
            backgroundColor: appTheme.scaffoldBackgroundColor,
            activeColor: appTheme.accentColor,
            inactiveColor: Colors.blueGrey,)
        )
      ),
      bottom: 30);
  }
}

class PinterestGrid extends StatefulWidget {

  @override
  _PinterestGridState createState() => _PinterestGridState();
}

class _PinterestGridState extends State<PinterestGrid> {
  ScrollController controller = new ScrollController();

  @override
  void initState(){
      double scrollanterior = 0;
    controller.addListener(() {
      if (controller.offset > scrollanterior && controller.offset > 20){
        Provider.of<_MenuModel>(context, listen: false).mostrar = false;
      }else{
        Provider.of<_MenuModel>(context, listen: false).mostrar = true;
      }
      scrollanterior = controller.offset;
    });
    super.initState();
  }
  @override
  void dispose(){
    controller.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    final List items = List.generate(200, (i) => i);
    return new StaggeredGridView.countBuilder(
      controller: controller,
      crossAxisCount: 4,
      itemCount: items.length,
      itemBuilder: (BuildContext context, int index) => _PinterestItem(index),
      staggeredTileBuilder: (int index) =>
      new StaggeredTile.count(2, index.isEven ? 2 : 4),
  mainAxisSpacing: 4.0,
  crossAxisSpacing: 4.0,
  );
  }
}

class _PinterestItem extends StatelessWidget {
  final int index;
  _PinterestItem(this.index);

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.all(5),
        decoration: BoxDecoration(
        color: Colors.blue,
        borderRadius: BorderRadius.all(Radius.circular(30))
        ),
        child: new Center(
          child: new CircleAvatar(
            backgroundColor: Colors.white,
            child: new Text('$index'),
          ),
        ));
  }
}

class _MenuModel with ChangeNotifier{
  bool _mostrar = true;
  bool get mostrar => this._mostrar;
  set mostrar(bool valor){
    this._mostrar = valor;
    notifyListeners();
  }
}