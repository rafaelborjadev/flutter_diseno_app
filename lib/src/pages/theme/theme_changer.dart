import 'package:flutter/material.dart';

class ThemeChanger with ChangeNotifier{
  bool _darkTheme = false;
  bool _customTheme = false;
  
  ThemeData _currentTheme;

  /* Esto es para cargar el thema por defecto desde el launcher. Esto se puede implementar con un userPreferences */
  ThemeChanger(int theme){
    switch (theme) {
      case 1: /* Light */
        _darkTheme = false;
        _customTheme = false;
        _currentTheme = ThemeData.light();
        break;
      case 2: /* Dark */
        _darkTheme = true;
        _customTheme = false;
        _currentTheme = ThemeData.dark();
        break;
      case 3: /* Custom */
        _darkTheme = false;
        _customTheme = false;
        _currentTheme = ThemeData.light();
        break;
      default:
    }
  }

  bool get darkTheme => this._darkTheme;
  bool get customTheme => this._customTheme;
  ThemeData get currentTheme => this._currentTheme;
  set darkTheme (bool value){
    _customTheme = false;
    _darkTheme = value;
    (value) ? _currentTheme = ThemeData.dark() : _currentTheme = ThemeData.light();
    notifyListeners();
  }
  set customTheme (bool value){
    _customTheme = value;
    _darkTheme = false;
    (value) 
    ? _currentTheme = ThemeData.dark().copyWith(
      accentColor: Color(0xff48A0EB),
      primaryColorLight: Colors.white,
      scaffoldBackgroundColor: Color(0xff16202B),
      textTheme: TextTheme(
        bodyText1: TextStyle(color: Colors.white)
      )
    ) 
    : _currentTheme = ThemeData.light();

    notifyListeners();
  }
}