import 'package:diseno_app/src/pages/theme/theme_changer.dart';
import 'package:diseno_app/src/widgets/slideshow.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
class SlideShowPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);
    final widgets = [
          SvgPicture.asset('assets/svg/slide-1.svg'),
          SvgPicture.asset('assets/svg/slide-2.svg'),
          SvgPicture.asset('assets/svg/slide-3.svg'),
          SvgPicture.asset('assets/svg/slide-4.svg'),
          SvgPicture.asset('assets/svg/slide-5.svg')
    ];
    return Scaffold(
      body: SlideShow(
        slides: widgets,
        topDots: false,
        colorPrimario: (!appTheme.darkTheme) ? Color(0xffFF5A7E) : appTheme.currentTheme.accentColor,
        // colorSecundario: Colors.black,
        primaryBullet: 20,
        secondaryBullet: 12,
      ),
    );
  }
}