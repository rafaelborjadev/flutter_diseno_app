import 'package:flutter/material.dart';

class SliderModel with ChangeNotifier{
  double _currentPage = 0;

  double get currentpage => this._currentPage;

  set currentpage(double currentpage){
    _currentPage = currentpage;
    notifyListeners();
  }



}